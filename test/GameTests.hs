module GameTests where

import Test.Tasty
import Test.Tasty.HUnit

import qualified Mass
import qualified Ship
import qualified Bullet
import qualified MassHeading
import qualified Data.Set as Set
import Game
import World
import Linear

unitTests = [ testCase "hitShips_ships" $ hitShips_ships,
              testCase "hitShips_bullets" $ hitShips_bullets,
              testCase "stepWorld" $ stepWorld ]

mkShip :: Int -> Int -> Ship.Ship
mkShip id hp = Ship.Ship id MassHeading.mh1 1 hp 0

mkBullet :: Int -> Int -> Bullet.Bullet
mkBullet id atk = Bullet.Bullet id MassHeading.mh1 1 atk 1 0

_hitShips s b =
  let
    ss = Set.fromList [ s ]
    bs = Set.fromList [ mkBullet 0 10 ]
    (ss',bs') = Game.hitShips ss bs
  in
    (ss',bs')

hitShips_ships =
  let
    s = mkShip 0 100
    b = mkBullet 0 10
    (ss',bs') = _hitShips s b
    xs = Ship.damage (Bullet.atk b) s
    xss = Set.fromList [ xs ]
  in
    assertEqual "ship?" xss ss'

hitShips_bullets =
  let
    s = mkShip 0 100
    b = mkBullet 0 10
    (ss',bs') = _hitShips s b
  in
    assertEqual "bullet?" Set.empty bs'

stepWorld =
    let
      s = Ship.Ship 0 (MassHeading.MassHeading (Mass.Mass (V2 0 0) (V2 0 0) (V2 0 0) 1) 1) 1 100 0
      b = Bullet.Bullet 0 (MassHeading.MassHeading (Mass.Mass (V2 100 100) (V2 0 0) (V2 0 0) 1) 1) 1 1 1 0
      g = Game (World w h (Set.fromList [s]) (Set.fromList [b]))
      g' = step (Bullet.lifespan b) g
      Game (World w h ss' bs') = g'
    in
      assertEqual "bullet?" Set.empty bs'
