module ShipTests where

import Test.Tasty
import Test.Tasty.HUnit

import qualified Ship
import qualified MassHeading

unitTests = [ testCase "s1 step 0" $ s1Step0,
              testCase "s1 step 1" $ s1Step1,
              testCase "s1 alive" $ s1Alive,
              testCase "s1 dead" $ s1Dead ]

mkShip :: Int -> Int -> Ship.Ship
mkShip id hp = Ship.Ship id MassHeading.mh1 1 hp 0

s1Alive =
  let
    hp = 100
    s0 = mkShip 0 hp
  in
    assertBool "alive?" (Ship.alive s0)

s1Dead =
  let
    hp = 100
    s0 = mkShip 0 hp
    s1 = Ship.damage hp s0
  in
    assertBool "dead?" (not (Ship.alive s1))

s1Step0 =
  let
    hp = 100
    s0 = mkShip 0 hp
    s1 = Ship.step 0 s0
  in
    s0 @?= s1

s1Step1 =
  let
    hp = 100
    s0 = mkShip 0 hp
    s1 = Ship.step 1 s0
  in
    s0 @?= s1
