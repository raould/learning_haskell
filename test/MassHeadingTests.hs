module MassHeadingTests where

import Test.Tasty
import Test.Tasty.HUnit

import qualified MassHeading

unitTests = [ testCase "mh1 step 0" $ mh1Step0,
              testCase "mh1 step 1" $ mh1Step1 ]

mh1Step0 =
  let
    s0 = MassHeading.mh1
    s1 = MassHeading.step 0 s0
  in
    s0 @?= s1

mh1Step1 =
  let
    s0 = MassHeading.mh1
    s1 = MassHeading.step 1 s0
  in
    s0 @?= s1
