module BoundsTests where

import Test.Tasty
import Test.Tasty.HUnit

import qualified Bounds
import qualified Linear

unitTests = [ testCase "circles overlap" circlesOverlap,
              testCase "circle do not overlap" circlesDoNotOverlap,
              testCase "circles collection overlap" circlesCollectionOverlap,
              testCase "circle rectangle overlap" circleRectangleOverlap,
              testCase "circle do not rectangle overlap" circleRectangleDoNotOverlap ]

circlesOverlap =
  let
    b0 = Bounds.BCircle (Linear.V2 0 0) 5
    b1 = Bounds.BCircle (Linear.V2 10 0) 5
  in
    assertBool "overlap?" (Bounds.isOverlap b0 b1)

circlesDoNotOverlap =
  let
    b0 = Bounds.BCircle (Linear.V2 0 0) 4.9
    b1 = Bounds.BCircle (Linear.V2 10 0) 5
  in
    assertBool "do not overlap?" (not (Bounds.isOverlap b0 b1))

circlesCollectionOverlap =
  let
    as = [Bounds.BCircle (Linear.V2 0 0) 5, Bounds.BCircle (Linear.V2 10 0) 5]
    overlaps = Bounds.findOverlaps1 as
  in
    assertEqual ("overlap? " ++ show overlaps) 2 (length overlaps)

circleRectangleOverlap =
  let
    b = Bounds.BCircle (Linear.V2 0 0) 5
    r = Bounds.BRectangle (Linear.V2 0 0) (Linear.V2 1 1)
  in
    assertBool "overlap?" (Bounds.isOverlap b r)

circleRectangleDoNotOverlap =
  let
    b = Bounds.BCircle (Linear.V2 0 0) 5
    r = Bounds.BRectangle (Linear.V2 (-6) 0.1) (Linear.V2 (-5.01) 0)
  in
    assertBool "overlap?" (not (Bounds.isOverlap b r))
