import Test.Tasty
import Test.Tasty.HUnit

import qualified BoundsTests
import qualified MassHeadingTests
import qualified ShipTests
import qualified WorldTests
import qualified GameTests

main = defaultMain tests
tests = testGroup "Tests" [unitTestsGroup]
unitTestsGroup = testGroup "Unit Tests"
                 (BoundsTests.unitTests ++
                  MassHeadingTests.unitTests ++
                  ShipTests.unitTests ++
                  WorldTests.unitTests ++
                  GameTests.unitTests)
