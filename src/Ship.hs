module Ship where

import qualified Mass
import qualified MassHeading
import qualified Bounds
import Linear

data Ship = Ship {
  id :: Int,
  mh :: MassHeading.MassHeading,
  br :: Double,
  hp :: Int,
  dmg :: Int
  } deriving (Show, Eq, Ord)

instance Bounds.ToBounds Ship where
  getBounds (Ship _ mh br _ _) =
    Bounds.BCircle (Mass.pos (MassHeading.mass mh)) br

bounds :: Ship -> Bounds.Bounds
bounds ship =
  Bounds.BCircle (Mass.pos (MassHeading.mass (mh ship))) (br ship)

step :: Double -> Ship -> Ship
step dt (Ship id mh br hp dmg) =
  let
    mh' = MassHeading.step dt mh
  in
    Ship id mh' br hp dmg

alive :: Ship -> Bool
alive ship =
  (dmg ship) < (hp ship)

damage :: Int -> Ship -> Ship
damage d (Ship id mh br hp dmg) =
  Ship id mh br hp (d + dmg)



