module World where

import Data.Set as Set
import Ship
import Bullet

data World = World {
  w :: Int,
  h :: Int,
  ships :: Set Ship,
  bullets :: Set Bullet
  } deriving (Show)

addShip :: Ship -> World -> World
addShip ship (World w h s b) =
  World w h (insert ship s) b

removeShip :: Ship -> World -> World
removeShip ship (World w h s b) =
  World w h (delete ship s) b

addBullet :: Bullet -> World -> World
addBullet bullet (World w h s b) =
  World w h s (insert bullet b)

removeBullet :: Bullet -> World -> World
removeBullet bullet (World w h s b) =
  World w h s (delete bullet b)
