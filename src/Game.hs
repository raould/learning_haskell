module Game where

import World
import qualified Data.Set as Set
import Ship
import Bullet
import Bounds

data Game = Game {
  world :: World
  } deriving (Show)

applyHitsToShips :: Set.Set Ship -> [(Ship,Bullet)] -> Set.Set Ship
applyHitsToShips ss sbs =
  if Set.null ss then Set.empty
  else if sbs == [] then ss
  else 
    let
      (s,b):tl = sbs
      s' = Ship.damage (atk b) s
      ss' = (Set.insert s' (Set.delete s ss))
   in
      applyHitsToShips ss' tl

hitShips :: Set.Set Ship -> Set.Set Bullet -> (Set.Set Ship, Set.Set Bullet)
hitShips ss bs =
  let
    hits = findOverlaps2 ss bs
    ss' = applyHitsToShips ss hits
  in
    (ss', Set.empty)

step :: Double -> Game -> Game
step dt (Game (World w h s b)) =
  let
    -- todo: this is not the full game tick, currently just partial, for learning haskell.
    (s',b') = (s,b)
    s'' = Set.filter (\e -> Ship.alive e) (Set.map (\e -> Ship.step dt e) s')
    b'' = Set.filter (\e -> Bullet.alive e) (Set.map (\e -> Bullet.step dt e) b')
  in
    Game (World w h s'' b'')
