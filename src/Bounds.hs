module Bounds where

import Linear
import Data.Maybe
import Data.List as List
import Data.Set as Set

-- todo: 'smart constructor's to ensure lt,rb relation is enforced.
data Bounds = BCircle { center :: V2 Double, radius :: Double } |
              BRectangle { lt :: V2 Double, rb :: V2 Double }
            deriving (Show, Eq)

class (Eq b, Ord b) => ToBounds b where
  getBounds :: b -> Bounds
instance ToBounds Bounds where
  getBounds (BCircle c r) = BCircle c r
  getBounds (BRectangle lt rb) = BRectangle lt rb
instance Ord Bounds where
  (BCircle c0 r0) <= (BCircle c1 r1) = if c0 == c1 then r0 <= r1 else c0 <= c1 
  (BRectangle lt0 rb0) <= (BRectangle lt1 rb1) = (lerp 0.5 lt0 rb0) <= (lerp 0.5 lt1 rb1)
  (BRectangle lt rb) <= (BCircle c0 r0) = (lerp 0.5 lt rb) <= c0
  (BCircle c0 r0) <= (BRectangle lt rb) = c0 <= (lerp 0.5 lt rb)

-- todo: actually test this stuff somehow some day.
isOverlap :: Bounds -> Bounds -> Bool
isOverlap (BCircle c0 r0) (BCircle c1 r1) =
  (r0+r1)**2 >= (quadrance (c1 ^-^ c0))
isOverlap (BRectangle (V2 l0 t0) (V2 r0 b0)) (BRectangle (V2 l1 t1) (V2 r1 b1)) =
  not ((l0 > r1 || l1 > r0) || (t0 > b1 || t1 > b0))
isOverlap (BRectangle lt0 rb0) (BCircle c1 r1) =
  isOverlap (BCircle c1 r1) (BRectangle lt0 rb0)
isOverlap (BCircle c0 r0) (BRectangle lt rb) =
  let
    (V2 cx cy) = c0
    (V2 l t) = lt
    (V2 r b) = rb
    r2 = r0**2
  in
    if l >= cx && b >= cy && (r2 < (quadrance ((V2 l b) ^-^ c0))) then False
    else if l >= cx && t < cy && (r2 < (quadrance ((V2 l t) ^-^ c0))) then False
    else if r < cx && b >= cy && (r2 < (quadrance ((V2 r b) ^-^ c0))) then False
    else if r < cx && t < cy && (r2 < (quadrance ((V2 r t) ^-^ c0))) then False
    else True

-- todo: something besides O(n^2).
findOverlaps1 :: (ToBounds a) => [a] -> [(a,a)]
findOverlaps1 as =
  let
    tupler :: (ToBounds a) => a -> a -> Bool
    tupler a0 a1 = a0 /= a1 && isOverlap (getBounds a0) (getBounds a1)
  in
    [(a0,a1) | a0 <- as, a1 <- as, tupler a0 a1]

findOverlaps2 :: (ToBounds a, ToBounds b) => Set a -> Set b -> [(a,b)]
findOverlaps2 as bs =
  let
    tupler :: (ToBounds a, ToBounds b) => a -> b -> Bool
    tupler a b = isOverlap (getBounds a) (getBounds b)
  in
    [(a,b) | a <- Set.toList as, b <- Set.toList bs, tupler a b]

