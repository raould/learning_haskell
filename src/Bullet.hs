module Bullet where

import qualified Mass
import qualified MassHeading
import qualified Bounds
import Linear

-- todo: life should be a type of game tickes i.e. seconds.
data Bullet = Bullet {
  id :: Int,
  mh :: MassHeading.MassHeading,
  br :: Double,
  atk :: Int,
  lifespan :: Double,
  lifetime :: Double
  } deriving (Show, Eq, Ord)

instance Bounds.ToBounds Bullet where
  getBounds (Bullet _ mh br _ _ _) =
    Bounds.BCircle (Mass.pos (MassHeading.mass mh)) br

bounds :: Bullet -> Bounds.Bounds
bounds bullet =
  Bounds.BCircle (Mass.pos (MassHeading.mass (mh bullet))) (br bullet)

alive :: Bullet -> Bool
alive bullet =
  (lifespan bullet) < (lifetime bullet)

step :: Double -> Bullet -> Bullet
step dt (Bullet id mh br atk lifespan lifetime) =
  let
    mh' = MassHeading.step dt mh
  in
    Bullet id mh' br atk lifespan (lifetime - dt)



