module MassHeading where

import Mass
import Linear

data MassHeading = MassHeading {
  mass :: Mass,
  heading  ::  Double
  } deriving (Show, Eq, Ord)

mh1 :: MassHeading
mh1 = MassHeading (Mass (V2 0 0) (V2 0 0) (V2 0 0) 1) 0

step :: Double -> MassHeading -> MassHeading
step dt (MassHeading (Mass p v a m) h) =
  let
    pos' = p ^+^ (dt *^ v)
    vel' = v ^+^ (dt *^ a)
    acc' = (V2 0 0)
  in
    MassHeading (Mass pos' vel' acc' m) h


