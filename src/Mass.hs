module Mass where

import Linear

-- todo: figure out how to use Linear.Affine.Point for pos.
data Mass = Mass {
  pos :: V2 Double,
  vel :: V2 Double,
  acc :: V2 Double,
  mass :: Double
  } deriving (Show, Eq, Ord)

mh1 :: Mass
mh1 = Mass (V2 0 0) (V2 0 0) (V2 0 0) 1

step :: Double -> Mass -> Mass
step dt (Mass p v a m) =
  let
    pos' = p ^+^ (dt *^ v)
    vel' = v ^+^ (dt *^ a)
    acc' = (V2 0 0)
  in
    Mass pos' vel' acc' m


